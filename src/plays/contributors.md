### Contributors

[Dori Coblentz](http://www.doricoblentz.com) is a Marion L. Brittain Postdoctoral Fellow at the Georgia Institute of Technology and the director of the Jonson Character Cloud project. She specializes in early modern English drama, digital pedagogy, and the history of fencing. Her book, _Artful Devices: Fencing, Form, and Cognition on the Early Modern Stage_ (forthcoming from Edinburgh UP in 2022) explores the ways in which early moderns generated and transmitted practical knowledge about time. She's also published on this topic in a 2015 article in the _Journal for Early Modern Cultural Studies_, "Killing Time in _Titus Andronicus_: Timing, Rhetoric, and the Art of Defence" and in _Italian Studies_, "'Maister of al artificiall force and sleight': Tempo and Dissimulation in Castiglione's _Book of the Courtier_" (2018)

[David Coblentz](http://www.linkedin.com/in/david-coblentz-68b5935/) is a Member of Technical staff at Cohesity, Inc. and the Jonson Character Cloud's UI architect. 

[Mark Kaethler](http://markkaethler.com) is an instructor at Medicine Hat College and Assistant Director of Mayoral Shows for the Map of Early Modern London project, hosted at the University of Victoria. He is the co-editor of _Shakespeare's Language in Digital Media: Old Words, New Tools_ with Janelle Jenstad and Jennifer Roberts-Smith as well as the author of _Thomas Middleton's Plural Politics and Jacobean Drama_ (forthcoming 2021)

Thanks to the Georgia Tech students of English 1102: Defending Society for their help in compiling the _Bartholomew Fair_ portions of this resource, including
* [Ananya Jain](http://www.linkedin.com/in/ananya-jain-gatech-edu)
* Mira Kaufman

Thanks to students at Medicine Hat College for their contributions on _Sejanus His Fall_ including
* Shelby Bach
* Chance Burnham
* Mathew Eisenbarth
* Donna Goyer-Franz
* Jenna McDonnell
* Rokia Mohammed
* Cameron O'Bear
* Robert Rice
* Parkor Thomas
* Derek Whitson

